
rules = {
    'neutralpolitics': {
        'Rule 1': """This comment has been removed for violating [comment rule 1:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

>Be courteous to other users.  Name calling, sarcasm, demeaning language, or otherwise being rude or hostile to another user will get your comment removed.

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",

        'Rule 2': """This comment has been removed for violating [comment rule 2](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules) as it does not provide sources for its statements of fact.  If you edit your comment to link to sources, it can be reinstated.  For more on NeutralPolitics source guidelines, see [here.](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_sources)

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",
        'Rule 3': """This comment has been removed for violating [comment rule 3:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

> Be substantive. NeutralPolitics is a serious discussion-based subreddit. We do not allow bare expressions of opinion, low effort one-liner comments, jokes, memes, off topic replies, or pejorative name calling.

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",
        'Rule 4': """"This comment has been removed for violating [comment rule 4:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

>Address the arguments, not the person. The subject of your sentence should be "the evidence" or "this source" or some other noun directly related to the topic of conversation. "You" statements are suspect.
If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)"
"""
    },
    'neutralnews': {
        'Rule 1': 'bar',
        'Rule 2': 'foo',
        'Rule 3': 'foobar',
        'Rule 4': 'something'
    },
    'npmods': {
        'Rule 1': """This comment has been removed for violating [comment rule 1:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

>Be courteous to other users.  Name calling, sarcasm, demeaning language, or otherwise being rude or hostile to another user will get your comment removed.

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",

        'Rule 2': """This comment has been removed for violating [comment rule 2](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules) as it does not provide sources for its statements of fact.  If you edit your comment to link to sources, it can be reinstated.  For more on NeutralPolitics source guidelines, see [here.](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_sources)

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",
        'Rule 3': """This comment has been removed for violating [comment rule 3:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

> Be substantive. NeutralPolitics is a serious discussion-based subreddit. We do not allow bare expressions of opinion, low effort one-liner comments, jokes, memes, off topic replies, or pejorative name calling.

If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)
""",
        'Rule 4': """"This comment has been removed for violating [comment rule 4:](https://www.reddit.com/r/NeutralPolitics/wiki/guidelines#wiki_comment_rules)

>Address the arguments, not the person. The subject of your sentence should be "the evidence" or "this source" or some other noun directly related to the topic of conversation. "You" statements are suspect.
If you have any questions or concerns, please feel free [to message us.](https://www.reddit.com/message/compose?to=%2Fr%2FNeutralPolitics)"
"""
    }}
