#!/usr/bin/env python

# TODO: Implement source check on edited removed comment
# TODO: have bot post to /r/npmodbot for logs

import praw
from time import gmtime, strftime
import re
from userName import *  # user logon data from another files not synced on git
from ruleList import rules  # current rules list for NN & NP

current_date = strftime("%Y-%m-%d", gmtime())
urls = re.compile('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+')
rule = re.compile(r'(?:data:)')

reddit = praw.Reddit(user_agent=reddit_user_agent,
                     client_id=reddit_app_key,
                     client_secret=reddit_app_secret,
                     username=reddit_user_name,
                     password=reddit_user_password)
modlist = [""]
sublist = ["npmods"]  # , "neutralnews", "NeutralPolitics"]


def get_modlist(subreddit):
    """Pull the current modlist instead of hardcoding, pass to global list"""

    try:
        for Redditor in reddit.subreddit(subreddit).moderator():
            modlist.append(Redditor.name)
    except TypeError:
        pass


def get_recent_comment():
    for comm in reddit.redditor('NpModBot').comments.new(limit=1):
        return comm


def comment_reply(comment_id, reply):
    """Comment on the reply, leaving a distinguished reply with removal text"""

    reddit.comment(comment_id).reply(reply)
    reddit.comment(get_recent_comment()).mod.distinguish(how='yes', sticky=False)
    reddit.comment(comment_id).mod.remove(spam=False)


def get_reports(subreddit):
    """Go through and check the modlog reports, filter mod reports and send them to check_report() and then
     if not None pass to comment_reply()"""

    for comment_id in reddit.subreddit(subreddit).mod.reports():
        # if comment_id.author not in modlist:
        try:
            user = comment_id.mod_reports[0][1]
            report = comment_id.mod_reports[0][0]

            if user in modlist:
                reason = rules[subreddit.lower()][report.split(':', 1)[0]]
                if reason is not None:
                    comment_reply(comment_id, reason)

        except IndexError:
            pass


def check_inbox():
    """Check the inbox and get the original comment via .parent() calls"""

    unread_messages = []
    for item in reddit.inbox.unread(limit=None):
        unread_messages.append(item)

    for message in unread_messages:
        bot_message_id = reddit.comment(message.parent()).id
        initial_removed_comment = reddit.comment(bot_message_id.parent())
        if urls.findall(str(initial_removed_comment.body)):
            initial_removed_comment.mod.approve()
        else:
            message.mod.report('Please review')

        reddit.inbox.mark_read(message)


for sub in sublist:
    get_modlist(sub)
    get_reports(sub)
    check_inbox()
